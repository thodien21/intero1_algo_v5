#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//d�clarations des variables
int nJeton =10; //nombre de jetons
int envie = 1;

int positionBalle;

int LanceBalle()//La balle est plac�e al�atoirement dans la chambre
{
    srand(time(NULL));
    positionBalle = rand()%6;
    return 0;
}

//Lorsque le joueur a des jetons, il joue la partie Roulette
int Roulette()
{
    while((nJeton>0)&&(nJeton<100))
    {
        int nMise;
        int couleur;
        int resultat=-1;

        //Demander la couleur il voudrais mettre (noir ou rouge)
        printf("Quelle couleur vous metterez ?\n0 : Rouge\n1 : Noir\n");
        scanf("%d", &couleur);

        //Demander le nombre de jetons � mettre
        printf("Vous avez %d jetons. Votre mise : ", nJeton);
        scanf("%d", &nMise);

        //Le joueur peut mettre que des jetons qu'il poss�de et moins que 25 jetons
        while((nMise<1)||(nMise>nJeton)||(nMise>25))
        {
            printf("Cette mise n'est pas valide. Choisissez une autre somme : ");
            scanf("%d", &nMise);
        }

        //On affiche sa mise si elle est valid�e
        printf("Vous avez mis %d jetons\n", nMise);

        //Le r�sultat de ce tour:
        resultat = rand()%37; //Un nombre entre 0 et 36 est choisi al�atoirement
        printf("Le resultat est %d\n", resultat); //Affiche le nombre

        //V�rifier si il gagne ou perd sa mise
        if(resultat == 0)//Si le nombre choisit est 0, il perd quoi que sa mise est paire ou impaire
        {
            nJeton -= nMise;
            printf("Vous avez perdu %d jetons\n",nMise);
        }
        else
        {
            if(resultat%2 == couleur)//S'il mis juste
            {
                nJeton += nMise; //Il gagne sa mise
                printf("Vous avez gagne %d jetons\n",nMise);
            }
            else //s'il mis mal
            {
                nJeton -= nMise; //Il perd sa mise
                printf("Vous avez perdu %d jetons\n",nMise);
            }
        }

        printf("Vous avez maintenent %d jetons\n", nJeton);
    }

    return 0;
}

int RouletteRusse()
{
    //cr�ation du table de 6 cases
    int tab[6];
    int choix;
    for(int i=0; i<6; i++)
    {
        tab[i]=0;
    }
    tab[positionBalle]=1;

    //La balle se rapproche � la position 0 � chaque tour de Roulette Russe
    positionBalle--;

    //Si la balle est dans la chambre, le joueur mort
    if(positionBalle==0)
    {
        printf("CONDOLEANCE ! VOUS ETES MORT !\n");
        nJeton =0;
        return envie=0;
    }
    else//Si elle n'est pas encore dans la chambre
    {
        nJeton += 20;
        printf("Vous gagnez 20 jetons en plus, maintenant vous avez en total %d jetons", nJeton);
        if(nJeton >=100)
        {
            printf("\n\nBRAVO!!! VOUS AVEZ GAGNE !!!\n");
            nJeton =0;
            return envie=0;
        }
        printf("\nVoulez vous continuer quelle version ?\n1 : Roulette Normale\n0 : Roulette Russe\n");
        scanf("%d", &choix);
        switch(choix)
        {
        case 0:
            RouletteRusse();
        default:
            Roulette();
        }
    }
}

int main()
{
    //Placer al�atoirement la balle dans le barillet pour la partie Roulette Russe
    LanceBalle();

    //Le jeu poursuit tant qur le joueur est envie et qu'il n'a pas 100 jetons
    while((envie)&&(nJeton<100))
    {
        Roulette(); //Fin partie Roulette, soit le joueur n'a aucun jeton, soit il a au moins 100 jetons

        //Le jeu est gagne lorsque le joueur atteint >=100 jetons, il sort du jeu
        if(nJeton>=100)
        {
            printf("\n\nBRAVO!!! VOUS AVEZ GAGNE !!!\n");
            nJeton =0;
            return envie=0;
        }

        //Si le joueur n'a aucun jeton, il mise sa vie � la Roulette Russe
        printf("\nAttention! Vous passez a la Roulette Russe !\n");
        RouletteRusse();
    }
    return 0;
}
